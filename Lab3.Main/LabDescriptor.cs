﻿using System;
using Lab3.Contract;
using Lab3.Implementation;

namespace Lab3
{
    public struct LabDescriptor
    {
        #region definitions

        public delegate object GetInstance(object component);
        
        #endregion

        #region P1

        public static Type I1 = typeof(IDrzwi);
        public static Type I2 = typeof(IGuzik);
        public static Type I3 = typeof(Interface3);

        public static Type Component = typeof(Fasada);

        public static GetInstance GetInstanceOfI1 = Fasada.Drzwi;
        public static GetInstance GetInstanceOfI2 = Fasada.Guzik;
        public static GetInstance GetInstanceOfI3 = Fasada.IF3;
        
        #endregion

        #region P2

        public static Type Mixin = typeof(DomieszkaMixIn);
        public static Type MixinFor = typeof(IGuzik);

        #endregion
    }
}
