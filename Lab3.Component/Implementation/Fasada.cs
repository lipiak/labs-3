﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab3.Contract;
using Lab3.Implementation;

namespace Lab3.Implementation
{
    public class Fasada : IDrzwi, IGuzik, Interface3
    {
        

        public void OtworzDrzwi()
        {
           
        }

        public void ZamknijDrzwi()
        {
            
        }

        public void GuzikWewnetrzny()
        {
            
        }

        public void GuzikZewnetrzny()
        {
            
        }

        public void MetodaPierwsza()
        {
            
        }

        public void MetodaDruga()
        {
            
        }

        public static IDrzwi Drzwi(object component)
        {
            return new Drzwi();
        }

        public static IGuzik Guzik(object component)
        {
            return new Guzik();
        }

        public static Interface3 IF3(object component)
        {
            return new Klasa3();
        }
    }
}
